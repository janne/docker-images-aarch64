# VideoLAN aarch64 docker images

VideoLAN aarch64 docker images are used in GitLab CI jobs to provide static build environments.

On push, the [GitLab CI jobs] will launch and figure out the images that need to be rebuilt.

The results will be available on VideoLAN docker registry under registry.videolan.org:5000/$tag.

The suffix of -aarch64 will be appended when making an image name prior to
push.  I know it's horrible and hacky, but it's a good way to ensure we can
distinguish between amd64 and aarch64 images.

Also, make sure to use USER at the end of Dockerfile to ensure we're not running builds as root.

The image builder uses a hacked version of Google's [kaniko] to be able to
build images in a non-compromised security-wise environment (e.g. not in
docker-in-docker privileged mode).

   [Gitlab CI jobs]: <https://code.videolan.org/videolan/docker-images-aarch64/-/jobs>
   [kaniko]: <https://github.com/GoogleContainerTools/kaniko>

